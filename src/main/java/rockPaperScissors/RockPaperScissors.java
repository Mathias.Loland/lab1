package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
	public static boolean winner(String choice1,String choice2) {
		/*
		 * The code here checks two given arguments.
		 * At first, it will check if choice1 is equal to
		 * one of the strings mentioned below.
		 * Otherwise, the code will only check if
		 * choice2 is equal to "scissors" and
		 * return either true or false.
		 * If choice1 is equal to "paper" or "scissors",
		 * then, the code will return true or false
		 * based off if choice2 is equal to the
		 * return statement that follows choice1's.
		 * */
		if (choice1.equals("paper")) {
			return choice2.equals("rock");
		} else if (choice1.equals("scissors")) {
			return choice2.equals("paper");
		} else {
			return choice2.equals("scissors");
		}
	}
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    int endGame = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	while (true) {
    		int randomItem = new Random().nextInt(rpsChoices.size());
    		String computerTurn = rpsChoices.get(randomItem);
			String humanTurn;
	
			while(true) {
				System.out.println("Let's play round " + roundCounter);
				humanTurn = readInput("Your choice (Rock/Paper/Scissors)?");
				humanTurn = humanTurn.toLowerCase();
				
				if (humanTurn.equals("rock") || humanTurn.equals("paper") || humanTurn.equals("scissors")) {
					break;
				}
				System.out.println("I do not understand " + humanTurn + ". Could you try again?");
			}
		
			String choice = "Human chose " + humanTurn + ", computer chose " + computerTurn + ". ";			
			if (winner(humanTurn,computerTurn)) {
				System.out.println(choice + "Human wins!");
				humanScore++;
			} else if (winner(computerTurn,humanTurn)) {
				System.out.println(choice + "Computer wins!");
				computerScore++;
			} else {
				System.out.println(choice + "It's a tie!");
			} 			
  			System.out.println("Score: human " + humanScore + ", computer " + computerScore);
  			
  			while (true) {
  				String playAgain = readInput("Do you wish to continue playing? (y/n)?");
  				playAgain = playAgain.toLowerCase();

  				if (playAgain.equals("n")) {
  					endGame++;
  					break;
  				} else if (!playAgain.equals("y") & !playAgain.equals("n")) {
  					System.out.println("I do not understand " + playAgain + ". Could you try again?");
  				} else {
  					break;
  				}
  			}
  			if (endGame > 0) {
  				System.out.println("Bye bye :)");
  				break;
  			}
  			roundCounter++;
		}
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
